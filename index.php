<?require_once($_SERVER['DOCUMENT_ROOT'].'/local/controller.php'); ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <link rel="stylesheet" href="/local/css/style.css?v=<?=time();?>">
    <script type="text/javascript" src="/local/js/script.js?v=<?=time();?>"></script>
    <title><?=$arPages[$curPage]['title']?></title>
</head>
<body>
<header>
    <div class="menu">
        <ul>
            <?foreach($arPages as $url => $arItem):?>
                <?if($arItem['menu'] == 'Y'):?>
                    <li><a class="<?=($curPage == $url)?'active':''?>" href="/?page=<?=$url?>"><?=$arItem['title']?></a></li>
                <?endif;?>
            <?endforeach;?>
        </ul>
    </div>
</header>

<section>
<h1><?=$arPages[$curPage]['title']?></h1>
</section>


<!--<div>-->
    <?include_once($page);?>
<!--</div>-->


</body>
</html>