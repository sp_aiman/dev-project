<?
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/user.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/photo.php');

try {

    $user    = new User();
    $curUser = $user->getCurUser();
    $curPage = ($_GET['page']) ?: 'main';

    $arPages = array(
        'main'      => array('title' => 'Главная',          'menu' => 'Y'),
        'add_photo' => array('title' => 'Добавить фото',    'menu' => 'Y'),
        'my_photo'  => array('title' => 'Мои фото',         'menu' => 'Y'),
        'profile'   => array('title' => 'Профиль',          'menu' => 'Y'),
        '404'       => array('title' => '404 страница',     'menu' => 'N'),
    );

    if(!key_exists($curPage, $arPages))
        $curPage = '404';

    $page = $_SERVER['DOCUMENT_ROOT']."/local/pages/$curPage.php";

    if($curPage == 'main' || $curPage == 'my_photo') {
        $photo = new Photo();
        $arResult = array();
        if($curPage == 'main')
            $arResult = $photo->getPhoto();
        elseif($curPage == 'my_photo' && $curUser)
            $arResult = $photo->getPhoto($curUser);
        else
            $arResult = '';
    }

}catch (ErrorException $e) {
    print_r('test');
}
