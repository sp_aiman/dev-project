<?php

class Db
{
    private const dbName = 'ch39697_test';
    private const pass = 'pwgV4sX1';
    private static $db;

    private static function getDbConnection()
    {
        if(!self::$db) {
            self::$db = mysqli_connect('localhost', self::dbName, self::pass, self::dbName);
            mysqli_set_charset(self::$db, "utf8");
        }
        return  self::$db;
    }

    public static function addData(string $table, string $arFields, string $arValues)
    {
        $query = "INSERT INTO `$table`";
        $query .= " $arFields VALUES $arValues";

        return self::request($query, 'add');

    }

    public static function getData($table, $filter=''): array
    {
        $query = "SELECT * FROM `$table`";

        if($filter)
            $query .= " WHERE $filter";

        return self::request($query);

    }

    private static function request(string $query, $type='')
    {
        $connection = self::getDbConnection();

        $result   = mysqli_query($connection, $query);
        $arResult = array();

        if($type == 'add') {
            $arResult['status'] = $result;
        } elseif ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $arResult[] = $row;
            }
        }

        return $arResult;
    }
}