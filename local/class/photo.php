<?php

class Photo
{
    private string $dbTable = 'images';

    public function getPhoto($user = ''): array
    {
        if($user)
            $filter = "`user` = '$user'";

        return Db::getData($this->dbTable, $filter);
    }

    public function addPhoto($src, $user): array
    {

        $arFields = "(`src`, `user`)";
        $arValues = "('$src', '$user')";

        if( Db::addData($this->dbTable, $arFields, $arValues) ) {
            $result['result'] = "Изображение успешно добавлено";
            $result['status'] = 'ok';
        } else {
            $result['result'] = "Неизвестная ошибка";
            $result['status'] = 'error';
        }


        return $result;
    }
}