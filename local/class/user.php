<?php

class User
{
    private string $curUser = '';
    private string $dbTable = 'users';

    public function getCurUser()
    {
        if(!$this->curUser && $_SESSION['user'])
            $this->curUser = $_SESSION['user'];

        return $this->curUser;
    }

    private function checkUser($login, $pass = false)
    {
        $filter = " `login` = '$login'";

        if($pass)
            $filter .= "`password` = '$pass'";

        return count(Db::getData($this->dbTable, $filter)) > 0;
    }

    public function loginUser($login, $pass): array
    {
        $filter = " `login` = '$login'";
        if( count($arUserData = Db::getData($this->dbTable,$filter) ) > 0 ) {
            $result['test'] = $arUserData[0]['password'];
            if(password_verify($pass, $arUserData[0]['password'])) {
                $result['status']  = 'ok';
                $result['result'] = 'Авторизация прошла успешно';
                $this->setSessionData($arUserData[0]['login']);
            } else {
                $result['status']  = 'error';
                $result['result'] = 'Пароль введен неверно';
            }

        } else {
            $result['status']  = 'error';
            $result['result'] = 'Пользователь с таким логином не найден';
        }

        return $result;
    }

    public function addUser($login, $pass, $confirmPass): array
    {
        if( $this->checkUser($login) ) {
            $result['status']  = 'error';
            $result['result'] = 'Пользователь с таким логином уже существует';
        } elseif($pass !== $confirmPass ) {
            $result['status']  = 'error';
            $result['result'] = 'Пароли не идентичны';
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);

            $arFields = "(`login`, `password`)";
            $arValues = "('$login', '$pass')";

            if( Db::addData($this->dbTable, $arFields, $arValues) ) {
                $result['result'] = "Пользователь $login, успешно зарегистрирован";
                $result['status'] = 'ok';
                $this->setSessionData($login);
            } else {
                $result['result'] = "Неизвестная ошибка";
                $result['status'] = 'error';
            }
        }

        return $result;

    }

    private function setSessionData($userLogin)
    {
        $_SESSION['user'] = $userLogin;
    }

}