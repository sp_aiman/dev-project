<div class="gallery" id="gallery">
    <?foreach ($arResult as $arItem):?>
        <div class="gallery-item">
            <div class="content">
                <img src="<?=$arItem['src']?>" alt="<?=$arItem['user'];?>">
                <span><?=$arItem['user'];?></span>
            </div>
        </div>
    <?endforeach;?>
</div>
