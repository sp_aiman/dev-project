<?
session_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/db.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/user.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/local/class/photo.php');

$user   = new User();
$image  = new Photo();
$action = $_POST['action'];

$result['error']  = true;
$result['status'] = 'Ошибка';

switch ($action) {
    case 'registration':

        $result = $user->addUser($_POST['name'], $_POST['pass'], $_POST['confirmPass']);
        break;

    case 'login':

        $result = $user->loginUser($_POST['name'], $_POST['pass']);
        break;

    case 'addPhoto':
        $author    = ($_POST['name']) ?: 'anonim';
        $uploadDir = $_SERVER['DOCUMENT_ROOT']."/uploads/";
        $imageDir  = "/uploads/";

        foreach ($_FILES["images"]["error"] as $key => $error) {
            if ($error == UPLOAD_ERR_OK) {
                $src =$author.'-'.$_FILES['images']['name'][$key];

                if( move_uploaded_file($_FILES["images"]['tmp_name'][$key], $uploadDir.$src) ) {
                    $result[] = $image->addPhoto($imageDir.$src, $author);
                }
            }
        }

        $result['result'] = 'Фотографии успешно загружены';
        $result['status'] = 'error';
        break;

    default:
        break;
}

echo json_encode($result);

?>


